import datetime

from flask import Blueprint, render_template, abort, g, request, jsonify
from jinja2 import TemplateNotFound
import models

from .common import slug_to_date, get_min_ranges, r_diff

ticker = Blueprint(
    'ticker', 
    __name__,
    template_folder='templates'
    )

@ticker.route('/api/', defaults={'is_api': True},  methods=['GET'])
@ticker.route('/',  methods=['GET'])
def root(is_api=False):
    stocks = models.Stock.select()
    if is_api:
        return jsonify(list(stocks.dicts()))
    else:
        return render_template('stocks.j2', stocks=stocks)


@ticker.route(
    '/api/<stockname>/', 
    defaults={'is_api': True},  
    methods=['GET'],
    strict_slashes=True
    )
@ticker.route('/<stockname>/', methods=['GET'], strict_slashes=True)
def stock_view(stockname, months_ago=3, is_api=False):
    today = datetime.date.today()
    date_from = datetime.date(
        today.year,
        today.month - months_ago,
        today.day
        )
    stock = models.Stock.get_or_none(
        models.Stock.name == stockname.upper()
        )
    filtered_ticks = models.Ticks.select().where(
        models.Ticks.stock == stock,
        models.Ticks.date > date_from
    )
    if stock:
        if is_api:
            response = {
                'stock_name': stock.name,
                'ticks': list(filtered_ticks.dicts()),
            }
            return jsonify(response)
        else:
            return render_template('ticks.j2', stock_name=stock.name, stock_data=filtered_ticks)
    else:
        return abort(404)

@ticker.route(
    '/api/<stockname>/analytics',
    methods=['GET'],
    defaults={'is_api': True},
    strict_slashes=False
    )
@ticker.route('/<stockname>/analytics', methods=['GET'], strict_slashes=False)
def stock_analytics(stockname, days_ago = 100, is_api=False):
    date_from = request.args.get('date_from')
    date_to = request.args.get('date_to')
    try:
        if date_from:
            date_from = slug_to_date(date_from)
        else:
            date_from = datetime.date.today() - datetime.timedelta(
                days=days_ago
                )
        date_to = slug_to_date(date_to) if date_to else datetime.date.today()
    except Exception as error:
        return abort(422)

    stock = models.Stock.get_or_none(
        models.Stock.name == stockname.upper()
        )
    filtered_ticks = models.Ticks.select().where(
        models.Ticks.stock == stock,
        models.Ticks.date >= date_from,
        models.Ticks.date <= date_to
    ).order_by(models.Ticks.date.desc())

    diffs = []
    for i in range(len(filtered_ticks) - 1):
        diffs.append(
            {
                'date': filtered_ticks[i].date,
                'open': r_diff(
                    filtered_ticks[i].open_, 
                    filtered_ticks[i + 1].open_
                    ),
                'high': r_diff(
                    filtered_ticks[i].high, 
                    filtered_ticks[i + 1].high
                    ),
                'low': r_diff(
                    filtered_ticks[i].low, 
                    filtered_ticks[i + 1].low
                    ),
                'close': r_diff(
                    filtered_ticks[i].close, 
                    filtered_ticks[i + 1].close
                    )
            }
        )
    if stock:
        if is_api:
            response = {
                'stock_name': stock.name,
                'ticks': diffs
            }
            return jsonify(response)
        else:
            return render_template(
                'diff_ticks.j2',
                stock_name=stock.name, 
                stock_data=diffs
                )
    else:
        return abort(404)

@ticker.route(
    '/api/<stockname>/delta', 
    defaults={'is_api': True}, 
    methods=['GET'], 
    strict_slashes=False
    )
@ticker.route('/<stockname>/delta', methods=['GET'], strict_slashes=False)
def stock_delta(stockname, is_api=False):
    value = request.args.get('value', default = 10.0, type = float)
    type_ = request.args.get('type', default = 'high')
    if value and type_.lower() in ('open', 'high', 'low','close'):
        stock = models.Stock.get_or_none(
            models.Stock.name == stockname.upper()
            )
        if type_.lower() == 'open':
            type_ = 'open_'
        type_data_pool = [
            (x.date, getattr(x, type_)) for x in stock.ticks
            ]
        min_ranges = get_min_ranges(type_data_pool, float(value))

        if not stock:
            return abort(404)
        if is_api:
            response = {
                'stock_name': stock.name,
                'ranges': min_ranges
            }
            return jsonify(response)
        else:    
            return render_template(
                'ranges.j2', 
                stock_name=stock.name, 
                ranges=min_ranges
                )
    else:
        return abort(422)


@ticker.route(
    '/api/<stockname>/insider/', 
    defaults={'is_api': True},  
    methods=['GET']
    )
@ticker.route('/<stockname>/insider/',  methods=['GET'])
def insiders(stockname, is_api=False):
    stock_data = models.Stock.get_or_none(
        models.Stock.name == stockname.upper()
        )
    insiders = {x.insider.name: x.insider.slug for x in stock_data.trades}
    if stock_data:
        if is_api:
            response = {
                'stock_name': stock_data.name,
                'insider_trades': list(stock_data.trades.dicts())
            }
            return jsonify(response)
        else:
            return render_template(
                'insiders.j2',
                insiders=insiders,
                stock_data=stock_data
                )
    else:
        return abort(404)

@ticker.route(
    '/api/<stockname>/insider/<name>', 
    defaults={'is_api': True}, 
    methods=['GET']
    )
@ticker.route('/<stockname>/insider/<name>', methods=['GET'])
def insider(stockname, name, is_api=False):
    stock_data = models.Stock.get_or_none(
        models.Stock.name == stockname.upper()
        )
    insider = models.Insiders.get(models.Insiders.slug == name)
    trades = models.InsiderTrades.select().where(
        models.InsiderTrades.insider == insider,
        models.InsiderTrades.stock == stock_data
        )
    if stock_data:
        if is_api:
            response = {
                'insider': insider.name,
                'trades': list(trades.dicts())
            }
            return jsonify(response)
        else:
            return render_template('insider_detail.j2', insider=insider, trades=trades)
    else:
        return abort(404)