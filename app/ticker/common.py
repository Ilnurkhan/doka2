import datetime
import math


def slug_to_date(slug: str) -> datetime.date:
    return datetime.datetime.strptime(slug, '%Y-%m-%d').date()

def get_min_ranges(pool: list, limit=10) -> list:
    '''
    pool must be sorted by date
    '''
    pool.sort(key=lambda x: x[0])
    ranges, min_ranges = [], []
    for tick_num, tick in enumerate(pool):
        for other_tick in pool[tick_num + 1:]:
            if math.fabs(tick[1] - other_tick[1]) >= limit:
                if tick[1] - other_tick[1] > 0:
                    direction = 'down'
                else:
                    direction = 'up'
                ranges.append((tick[0], other_tick[0], direction))
                break
    points = {x[1] for x in ranges}
    for point in points:
        for range_ in reversed(ranges):
            if point == range_[1]:
                min_ranges.append(range_)
                break
    return sorted(min_ranges, key=lambda x: x[0])

def r_diff(value1: float, value2: float, ndigits=2) -> float:
    return round(value1 - value2, ndigits)