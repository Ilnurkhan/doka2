import argparse
import collections
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
import string

from bs4 import BeautifulSoup
import requests
import peewee

import models


def get_soup(url: str) -> BeautifulSoup:
    r = requests.get(url.lower())
    print('parse URL:', url)
    return BeautifulSoup(r.text, 'html.parser')

def get_stock_name_from_url(url: str) -> str:
    return url.split('/')[-2].upper()

def get_page_count(url: str, max_pages=10) -> dict:
    name = get_stock_name_from_url(url)
    soup = get_soup(url)
    pages = soup.find('ul', {'class': 'pager'}).find_all('li')
    last = int(pages[-3].text)
    count = last if last <= max_pages else max_pages
    return {'name': name, 'count': count}   

def parse_table(url: str, retry_count=3) -> dict:
    while retry_count > 0:
        retry_count -= 1
        try:
            name = get_stock_name_from_url(url)
            soup = get_soup(url)
            insider = False
            if 'insider' in url.lower():
                table = soup.find('table', {'class': 'certain-width'})
                insider = True
            else:
                table = soup.find(
                    'div', {'id':'quotes_content_left_pnlAJAX'}
                    ).find('table')

            rows = table.find_all('tr')
            column_names = [
                list(x.stripped_strings)[0] for x in rows.pop(0).find_all('th')
                ]
            data_rows = [
                [
                    x.text.strip() for x in row.find_all('td')
                    ] for row in rows
            ]
            return {
                    'name' : name,
                    'column_names': column_names,
                    'rows': data_rows if insider else data_rows[1:],
                    }
        except Exception as error:
            print('Error while parsing data: {}'.format(error))


def slugify(name: str) -> str:
    slug = ''
    last_ok = False
    acceptable = string.ascii_letters + string.digits
    for letter in name:
        if letter in acceptable:
            slug += letter.lower()
            last_ok = True
        else:
            if last_ok:
                slug += '-'
                last_ok = False
    if slug.endswith('-'):
        slug = slug[:-1]
    return slug


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prefix_chars='-')
    parser.add_argument('-N', type=int, default=4, help='Thread count')
    args = parser.parse_args()

    with open('tickers.txt', 'rt') as tickers:
        stocks = tickers.read().splitlines()

    with ThreadPoolExecutor(max_workers=args.N) as executor:
        parse_url = 'https://www.nasdaq.com/symbol/{}/historical'
        stock_data = executor.map(
            parse_table,
            [parse_url.format(z) for z in stocks]
            )

    for data in stock_data:
        stock, created = models.Stock.get_or_create(name=data.get('name').upper())
        for tick in data.get('rows'):
            open_, high, low, close = [float(i) for i in tick[1:-1]]
            try:
                models.Ticks.create(
                    stock=stock,
                    date=datetime.strptime(tick[0], '%m/%d/%Y').date(),
                    open_=open_,
                    high=high,
                    low=low,
                    close=close,
                    volume=tick[5].replace(',', '')
                )
            except Exception as error:
                print('error while writing data {}'.format(error))
                models.db.rollback()
    
    with ThreadPoolExecutor(max_workers=args.N) as executor:
        parse_url = 'http://www.nasdaq.com/symbol/{}/insider-trades'
        page_counts = executor.map(
            get_page_count,
            [parse_url.format(z) for z in stocks]
            )
        urls = []
        for pc in page_counts:
            for page in range(1, pc.get('count')+1):
                urls.append(
                    parse_url.format(pc.get('name')) + '?page={}'.format(page)
                    )
        insider_data = executor.map(parse_table, urls)
        insider_data = list(insider_data)
        for data in insider_data:
            for row in data.get('rows'):
                insider, created = models.Insiders.get_or_create(
                    name=row[0],
                    slug=slugify(row[0]),
                    relation=row[1]
                )
                try:
                    models.InsiderTrades.create(
                        insider = insider,
                        stock = models.Stock.get(
                            models.Stock.name == data.get('name')
                            ),
                        last_date =datetime.strptime(
                            row[2],
                            '%m/%d/%Y'
                            ).date(),
                        transaction_type =row[3],
                        owner_type=row[4],
                        shares_traded = row[5].replace(',',''),
                        last_price = row[6] if row[6] else None,
                        shares_held = row[7].replace(',',''),
                    )
                except peewee.IntegrityError as error:
                    print('error while writing data {}'.format(error))
                    models.db.rollback()