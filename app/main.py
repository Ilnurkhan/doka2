from flask import Flask, g
from peewee import *

from ticker.ticker_view import ticker
import models

app = Flask(__name__)
app.register_blueprint(ticker)


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=True
        )