import peewee
import settings


db = peewee.PostgresqlDatabase(
    settings.DATABASE,
    user=settings.DB_USER,
    host=settings.DB_HOST,
    port=settings.DB_PORT
    )
    
class BaseModel(peewee.Model):
    class Meta:
        database = db


class Stock(BaseModel):
    name = peewee.CharField(unique=True)


class Ticks(BaseModel):
    stock = peewee.ForeignKeyField(Stock, backref='ticks')
    date = peewee.DateField()
    open_ = peewee.FloatField()
    high = peewee.FloatField()
    low = peewee.FloatField()
    close = peewee.FloatField()
    volume = peewee.IntegerField()

    class Meta:
        indexes = (
            (('stock', 'date'), True),
        )


class Insiders(BaseModel):
    name = peewee.CharField()
    slug = peewee.CharField()
    relation = peewee.CharField()
    
    class Meta:
        indexes = (
            (('name', 'relation'), True),
        )


class InsiderTrades(BaseModel):
    insider = peewee.ForeignKeyField(Insiders, backref='trades')
    stock = peewee.ForeignKeyField(Stock, backref='trades')
    last_date = peewee.DateField()
    transaction_type = peewee.CharField()
    owner_type = peewee.CharField()
    shares_traded = peewee.IntegerField()
    last_price = peewee.FloatField(null=True)
    shares_held = peewee.IntegerField()
    
    class Meta:
        indexes = (
            (('insider', 'stock', 'last_date',
             'transaction_type', 'shares_traded',
              'last_price', 'shares_held'), True),
        )


def create_tables():
    with db:
        db.create_tables([Stock, Ticks, Insiders, InsiderTrades])

